const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid, stringify, v4 } = require("uuid");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */
  /* YOUR CODE GOES HERE */
  /* ******************* */

  /* Criação das funções responsávies por detectar erros */
  /* Erro 400 */
  function error400(req, res, next){
    let user = req.body

    /* Erros de Contrato */
    //Verifica se o nome está presente
    if(!user.name){
      res.status(400).json({
        "error": "Request body had missing field {name}",
      })
    }
    //Verifica se o e-mail está presente
    if(!user.email){
      res.status(400).json({
        "error": "Request body had missing field {email}",
      })
    }
    //Verifica se a senha está presente
    if(!user.password){
      res.status(400).json({
        "error": "Request body had missing field {password}",
      })
    }
    //Verifica se a confirmação de senha está presente
    if(!user.passwordConfirmation){
      res.status(400).json({
        "error": "Request body had missing field {passwordConfirmation}",
      })
    }

    /* Erros de Formatação */
    user.name.split(" ").forEach(namePart => {
      //Verifica se cada nome inicia com maiúscula e é seguido de letras minúsculas
      if(namePart[0].match("[^A-Z]")||namePart.slice(1).match("[^a-z]")){
        res.status(400).json({
          "error": "Request body had malformed field {name}",
        })
      }
    })
    let regexp = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/
    //Verifica se a formatação do e-mail está correta
    if(!regexp.test(user.email)){
      res.status(400).json({
        "error": "Request body had malformed field {email}",
        })
      }
    let senha = user.password
    //Verifica se a senha possui o tamanho e os caracteres permitidos
    if(senha.length < 8 || senha.length > 32 || senha.match("[^a-zA-Z0-9]")){
        res.status(400).json({
          "error": "Request body had malformed field {password}",
        })
      }
    next()
  }

  /* Erro 422 */
  function error422(req, res, next){
    //Verifica se a senha e a confirmação de senha são iguais
    if(req.body.password!=req.body.passwordConfirmation){
      res.status(422).json({
        "error": "Password confirmation did not match",
      })
    }
    next()
  }

  /* Erro 401 */
  function error401(req, res, next){
    //Verifica se está presente o token de autenticação
    if(!req.headers.authentication){
      res.status(401).json({
        "error": "Access Token not found",
      })
    }
    next()
  }

  /* Erro 403 */
  function error403(req, res, next){
    //Descarta a string "Bearer" para obter apenas o token de autenticação
    let token = jwt.decode(req.headers.authentication.split(" ")[1])
    //Verifica se o token de autenticação e o UUID do usuário a ser
    //deletado são iguais
    if(token.id != req.params.uuid){
      res.status(403).json({
        "error": "Access Token did not match User ID",
      })
    }
    next()
  }

  /* Aqui se inicia de fato a implementação do CRUD */

  const database = mongoClient.db('EP1')
  const users_collection = database.collection('users')

  /* Criação do usuário */
  api.post("/users", error400, error422, (req, res) => {
    let user = req.body
    id = uuid()
    //Confirma a criação do usuário, ao passar por todos os
    //requisitos contidos nos middlewares
    res.status(201).json({
      "user": {
        "id": id,
        "name": user.name,
        "email": user.email,
      }
    })

    users_collection.findOne()
    
    //Cria o JSON a ser publicado no tópico do nats streaming
    userJSON = {
      "eventType": "UserCreated",
      "entityId": id,
       "entityAggregate": {
         "name": user.name,
         "email": user.email,
         "password": user.password,
       }
    }
    
    //Notifica a criação do usuário no tópico do nats streaming
    stanConn.publish('crud', JSON.stringify(userJSON), (err, guid) => {
       if(err){
         console.log('Erro na publicação: ' + err)
       }else{
         console.log('Publicação bem sucedida: ' + guid)
       }
     })
    })

  /* Deleção do usuário */
  api.delete("/users/:uuid", error401, error403, (req, res) => {
    //Confirma a remoção do usuário, ao passar por todos os
    //requisitos contidos nos middlewares
    res.status(200).json({
      "id": req.params.uuid,
    })

    //Cria o JSON a ser publicado no tópico do nats streaming
    let deletedUser_JSON = {
      "eventType": "UserDeleted",
      "entityId": req.params.uuid,
      "entityAggregate": {}
    }
    
    //Notifica a remoção do usuário no tópico do nats streaming
    stanConn.publish('crud', JSON.stringify(deletedUser_JSON), (err, guid) => {
      if(err){
        console.log('Erro na publicação: ' + err)
      }else{
        console.log('Publicação bem sucedida: ' + guid)
      }
    })
  })

  return api;
};

const stan = require("node-nats-streaming");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    /* ******************* */
    /* YOUR CODE GOES HERE */
    /* ******************* */

    //Cria as configurações da comunicação para se inscrever no canal 'crud'
    const opts = conn.subscriptionOptions().setStartWithLastReceived()
    const subs = conn.subscribe('crud', opts)

    //Exibe a mensagem recebida
    subs.on("message", (msg) => {
      console.log(JSON.parse(msg.getData()))
    })
  });

  return conn;
};
